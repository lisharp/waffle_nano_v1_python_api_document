# Python3 模块



在前面的几个章节中我们基本上是用 python 解释器来编程，如果你从 Python 解释器退出再进入，那么你定义的所有的方法和变量就都消失了。

为此 Python 提供了一个办法，把这些定义存放在文件中，为一些脚本或者交互式的解释器实例使用，这个文件被称为模块。

---

## import语句
想使用 Python 源文件，只需在另一个源文件里执行 import 语句，语法如下：

```python
import module1[, module2[,... moduleN]
```

当解释器遇到 import 语句，如果模块在当前的搜索路径就会被导入。

一个模块只会被导入一次，不管你执行了多少次import。这样可以防止导入模块被一遍又一遍地执行。

搜索路径是在Python编译或安装的时候确定的，安装新的库应该也会修改。做一个简单的实验，我们在桌面创建一个文本文件 `(fibo.txt)`，打开后输入:

```python
def fib(n):    # 定义到 n 的斐波那契数列
    a, b = 0, 1
    while b < n:
        print(b, end=' ')
        a, b = b, a+b
    print()
 
def fib2(n): # 返回到 n 的斐波那契数列
    result = []
    a, b = 0, 1
    while b < n:
        result.append(b)
        a, b = b, a+b
    return result
```

然后将文本文件保存后关闭，并重命名为 `fibo.py`。

<center><img src="assets/image/1.png" width="300"/></center>

然后打开我们的在线编程界面，选择右上角更多中的上传文件，将刚刚创建的 `fibo.py` 上传至开发板中，如下图所示：


<center><img src="assets/image/2.png" width="1000"/></center>

<center><img src="assets/image/3.png" width="1000"/></center>


<center><img src="assets/image/4.png" width="1000"/></center>

然后进入Python解释器，使用下面的命令导入这个模块：

```python
import fibo
```

这样做并没有把直接定义在fibo中的函数名称写入到当前符号表里，只是把模块fibo的名字写到了那里。

可以使用模块名称来访问函数： 

```python
>>>fibo.fib(1000)
1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
>>> fibo.fib2(100)
[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
>>> fibo.__name__
'fibo'
```
如果你打算经常使用一个函数，你可以把它赋给一个本地的名称：

```python
>>> fib = fibo.fib
>>> fib(500)
1 1 2 3 5 8 13 21 34 55 89 144 233 377
```


