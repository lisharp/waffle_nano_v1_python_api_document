# Python3 集合

- [集合的基本操作](#集合的基本操作)
    - [添加元素](#添加元素)
    - [移除元素](#移除元素)
    - [计算集合元素个数](#计算集合元素个数)
    - [清空集合](#清空集合)
    - [判断元素是否在集合中存在](#判断元素是否在集合中存在)
    - [集合内置方法完整列表](#集合内置方法完整列表)

&emsp;&emsp;集合`（set）`是一个无序的不重复元素序列。

&emsp;&emsp;可以使用大括号 `{ }` 或者 `set()` 函数创建集合，注意：创建一个空集合必须用 `set()` 而不是 `{ }`，因为 `{ }` 是用来创建一个空字典。

&emsp;&emsp;创建格式：

```
parame = {value01,value02,...}
或者
set(value)
```

实例如下：

```shell
>>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
>>> print(basket)                      # 这里演示的是去重功能
{'orange', 'banana', 'pear', 'apple'}
>>> 'orange' in basket                 # 快速判断元素是否在集合内
True
>>> 'crabgrass' in basket
False

>>> # 下面展示两个集合间的运算.
...
>>> a = set('abracadabra')
>>> b = set('alacazam')
>>> a                                  
{'a', 'r', 'b', 'c', 'd'}
>>> a - b                              # 集合a中包含而集合b中不包含的元素
{'r', 'd', 'b'}
>>> a | b                              # 集合a或b中包含的所有元素
{'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}
>>> a & b                              # 集合a和b中都包含了的元素
{'a', 'c'}
>>> a ^ b                              # 不同时包含于a和b的元素
{'r', 'd', 'b', 'm', 'z', 'l'}
```

&emsp;&emsp;类似列表推导式，同样集合支持集合推导式 `(Set comprehension)`:

```shell
>>> a = {x for x in 'abracadabra' if x not in 'abc'}
>>> a
{'r', 'd'}
```

---

## 集合的基本操作


### 添加元素

语法格式如下：

```python
s.add( x )
```

&emsp;&emsp;将元素 `x` 添加到集合 `s` 中，如果元素已存在，则不进行任何操作。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> thisset.add("Facebook")
>>> print(thisset)
{'Google', 'Facebook', 'Waffle', 'Taobao'}
```

&emsp;&emsp;还有一个方法，也可以添加元素，且参数可以是列表，元组，字典等，语法格式如下：

```python
s.update( x )
```

&emsp;&emsp;x 可以有多个，用逗号分开。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> thisset.update({1,3})
>>> print(thisset)
{1, 'Google', 3, 'Waffle', 'Taobao'}
>>> thisset.update([1,4],[5,6])  
>>> print(thisset)
{6, 1, 'Waffle', 3, 4, 'Google', 'Taobao', 5}
>>>
```

---

### 移除元素

语法格式如下：

```python
s.remove( x )
```

&emsp;&emsp;将元素 x 从集合 s 中移除，如果元素不存在，则会发生错误。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> thisset.remove("Taobao")
>>> print(thisset)
{'Google', 'Waffle'}
>>> thisset.remove("Facebook")   # 不存在会发生错误
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: Facebook
>>>
```

&emsp;&emsp;此外还有一个方法也是移除集合中的元素，且如果元素不存在，**不会** 发生错误。格式如下所示：

```python
s.discard( x )
```

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> thisset.discard("Facebook")  # 不存在不会发生错误
>>> print(thisset)
{'Taobao', 'Google', 'Waffle'}
```

&emsp;&emsp;我们也可以设置 **随机** 删除集合中的一个元素，语法格式如下：

```python
s.pop()
```

```python
thisset = set(("Google", "Waffle", "Taobao", "Facebook"))
x = thisset.pop()

print(x)
```

输出结果：

```shell
Google
```

&emsp;&emsp;多次执行测试结果都不一样。

&emsp;&emsp;set 集合的 pop 方法会对集合进行无序的排列，然后将这个无序排列集合的左面第一个元素进行删除。

---

### 计算集合元素个数

语法格式如下：

```python
len(s)
```

计算集合 s 元素个数。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao", "Facebook"))
>>> len(thisset)
4
```

---

### 清空集合

语法格式如下：

```python
s.clear()
```

清空集合 s。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> thisset.clear()
>>> print(thisset)
set()
```

### 判断元素是否在集合中存在

语法格式如下：

```python
x in s
```

判断元素 x 是否在集合 s 中，存在返回 True，不存在返回 False。

```shell
>>> thisset = set(("Google", "Waffle", "Taobao"))
>>> "Waffle" in thisset
True
>>> "Facebook" in thisset
False
>>>
```





