# Python3 输入和输出

- [输出格式美化](#输出格式美化)
- [旧式字符串格式化](#旧式字符串格式化)
- [读取键盘输入](#读取键盘输入)

---

## 输出格式美化

Python两种输出值的方式: 表达式语句和 print() 函数。

第三种方式是使用文件对象的 write() 方法，标准输出文件可以用 sys.stdout 引用。

如果你希望输出的形式更加多样，可以使用 str.format() 函数来格式化输出值。

如果你希望将输出的值转成字符串，可以使用 repr() 或 str() 函数来实现。

- str()： 函数返回一个用户易读的表达形式。
- repr()： 产生一个解释器易读的表达形式。

实例如下：

```python
>>> s = 'Hello, Waffle'
>>> str(s)
'Hello, Waffle'
>>> repr(s)
"'Hello, Waffle'"
>>> str(1/7)
'0.14285714285714285'
>>> x = 10 * 3.25
>>> y = 200 * 200
>>> s = 'x 的值为： ' + repr(x) + ',  y 的值为：' + repr(y) + '...'
>>> print(s)
x 的值为： 32.5,  y 的值为：40000...
>>> #  repr() 函数可以转义字符串中的特殊字符
... hello = 'hello, Waffle\n'
>>> hellos = repr(hello)
>>> print(hellos)
'hello, Waffle\n'
>>> # repr() 的参数可以是 Python 的任何对象
... repr((x, y, ('Google', 'Waffle')))
"(32.5, 40000, ('Google', 'Waffle'))"
```

注意：在第一个例子中, 每列间的空格由 print() 添加。

str.format() 的基本使用如下:

```python
>>> print('{} "{}!"'.format('WaffleNano', 'WOW'))
WaffleNano "WOW!"
```

位置及关键字参数可以任意的结合: 

```python
>>> print('站点列表 {0}, {1}, 和 {other}。'.format('Google', 'Waffle', other='Taobao'))
Google, Waffle,  Taobao
```

`!a` (使用 `ascii()`), `!s` (使用 `str()`) 和 `!r` (使用 `repr()`) 可以用于在格式化某个值之前对其进行转化:

```python
>>> import math
>>> print('常量 PI 的值近似为： {}。'.format(math.pi))
常量 PI 的值近似为： 3.141592653589793。
>>> print('常量 PI 的值近似为： {!r}。'.format(math.pi))
常量 PI 的值近似为： 3.141592653589793。
```

可选项 `:` 和格式标识符可以跟着字段名。 这就允许对值进行更好的格式化。 下面的例子将 `Pi` 保留到小数点后三位：

```python
>>> import math
>>> print('常量 PI 的值近似为 {0:.3f}。'.format(math.pi))
常量 PI 的值近似为 3.142。
```

在 `:` 后传入一个整数, 可以保证该域至少有这么多的宽度。 用于美化表格时很有用。 

```python
>>> table = {'Google': 1, 'Waffle': 2, 'Taobao': 3}
>>> for name, number in table.items():
...     print('{0:10} ==> {1:10d}'.format(name, number))
...
Taobao     ==>          3
Waffle     ==>          2
Google     ==>          1
```

也可以通过在 table 变量前使用 `**` 来实现相同的功能：

```python
>>> table = {'Google': 1, 'Waffle': 2, 'Taobao': 3}
>>> print('Waffle: {Waffle:d}; Google: {Google:d}; Taobao: {Taobao:d}'.format(**table))
Waffle: 2; Google: 1; Taobao: 3
```

---

## 旧式字符串格式化

`%` 操作符也可以实现字符串格式化。 它将左边的参数作为类似 sprintf() 式的格式化字符串, 而将右边的代入, 然后返回格式化后的字符串. 例如: 

```python
>>> import math
>>> print('常量 PI 的值近似为：%5.3f。' % math.pi)
常量 PI 的值近似为：3.142。
```

因为 str.format() 是比较新的函数， 大多数的 Python 代码仍然使用 % 操作符。但是因为这种旧式的格式化最终会从该语言中移除, 应该更多的使用 str.format(). 

--- 

## 读取键盘输入

Python 提供了 input() 内置函数从标准输入读入一行文本，默认的标准输入是键盘。


```python
str = input("请输入：")
print ("你输入的内容是: ", str)
```

这会产生如下的对应着输入的结果： 

```
请输入：Waffle Nano
你输入的内容是:  Waffle Nano
```
---

