# 二维码生成库

## 简介

&emsp;&emsp;使用此库可将普通字符串转化为二维码。

## qrcode API详解

&emsp;&emsp;使用`import qrcode`导入二维码生成类 `qrcode`。
&emsp;&emsp;再使用 `TAB` 按键来查看 `qrcode` 中所包含的内容：

```python
>>> import qrcode
>>>qrcode.
_name           ECC_HIGH        ECC_LOW              ECC_MEDIUM
ECC_QUARTILE       QrCode
```


### 宏定义

&emsp;&emsp;下面的宏定义用于配置qrcode，用于设置二维码的容错率。

| 宏定义        | 含义             |
| ------------- | --------------- |
| qrcode.ECC_LOW      | 容错率 7%  |
| qrcode.ECC_MEDIUM      | 容错率 15%  |
| qrcode.ECC_QUARTILE      | 容错率 25%  |
| qrcode.ECC_HIGH      | 容错率 30%  |


### 类

&emsp;&emsp;class QrCode()

### 构造QrCode

&emsp;&emsp;示例：

```python
import qrcode
qr = qrcode.QrCode() ##构造一个二维码生成对象
```

### 函数

#### qrcode.encode_text(str,ecc)

&emsp;&emsp;函数说明：生成一个二维码。

&emsp;&emsp;参数说明：

- `str`：待转化为二维码的字符串
- `ecc`：转化容错率

&emsp;&emsp;示例：

```python
import qrcode
qr = qrcode.QrCode()  ## 构造一个二维码生成对象
qr.encode_text("https://blackwalnut.tech", qrcode.ECC_HIGH)   ## 使用30%的容错率，将`https://blackwalnut.tech`字符串转化为二维码
```



#### qrcode.get_size()

&emsp;&emsp;函数说明：获取生成的正方形二维码的以像素点为单位的边长。

&emsp;&emsp;函数返回：返回生成的正方形二维码的以像素点为单位的边长。

&emsp;&emsp;示例：

```python

import qrcode
qr = qrcode.QrCode()  ## 构造一个二维码生成对象
qr.encode_text("https://blackwalnut.tech", qrcode.ECC_HIGH)   ## 使用30%的容错率，将`https://blackwalnut.tech`字符串转化为二维码
lens=qr.get_size() ##获取生成的正方形二维码的以像素点为单位的边长。

```


#### qrcode.get_module(col，row)


&emsp;&emsp;函数说明：返回给定坐标处像素的颜色。

&emsp;&emsp;参数说明：

- `col`：坐标像素列
- `row`：坐标像素行

&emsp;&emsp;函数返回：若值为`False`,代表此坐标所代表的像素点为白色或者坐标超出给定边界，若值为`True`,代表此坐标所代表的像素点为黑色。


&emsp;&emsp;示例：

```python
import qrcode
qr = qrcode.QrCode()  ## 构造一个二维码生成对象
qr.encode_text("https://blackwalnut.tech", qrcode.ECC_HIGH)   ## 使用30%的容错率，将`https://blackwalnut.tech`字符串转化为二维码
lens=qr.get_size() ##获取生成的正方形二维码的以像素点为单位的边长。
color=qr.get_module(5,4) ##获取生成的二维码在5列，4行处的颜色。
```



### 总示例代码

&emsp;&emsp;该代码能在屏幕上显示出最终完成转换的二维码图案。

```python
from machine import SPI, Pin
import st7789
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.fill(0xFFFFF)

import qrcode

qr = qrcode.QrCode()
qr.encode_text("https://blackwalnut.tech", qrcode.ECC_HIGH)

border = 4
len = 8

for row in range(0, qr.get_size()):
    for col in range(0, qr.get_size()):
        x1 = col*len+border
        y1 = row*len+border
        display.fill_rect(x1, y1, len, len, 0 if qr.get_module(col, row) else 0xFFFFF)
        print("##" if qr.get_module(col, row) else "  ", end = "")
    print()
print()

```








