# ADC 模数转换器

- [概要](#概要)
- [ADC是什么](#adc是什么)
- [ADC引脚定义](#adc引脚定义)
- [machine.ADC API详解](#machineadc-api详解)
- [宏定义](#宏定义)
- [类](#类)
- [构建ADC](#构建adc)
- [函数](#函数)
    - [设置平均算法采样次数](#设置平均算法采样次数)
    - [ADC测量电压范围](#ADC测量电压范围)
    - [ADC采样](#adc采样)
- [示例](#示例)

## 概要

&emsp;&emsp;本文档介绍 `ADC` 的概念，以及 `OpenHarmony` 中 `Python` 的 `ADC` 接口。`Waffle NanoV1` 的 `ADC` 模块具有以下功能特点：

- 输入时钟 3MHz，12bit 采样精度，单通道采样频率<200KHz。
- 共 8 个通道，支持软件配置 0～7 任意通道使能，逻辑按通道编号先低后高发起切换，每个通道采样 1 个数，通道切换时会使 ADC 电路进行 1 次复位处理。
- 支持对 ADC 采样数据进行平均滤波处理，平均次数支持 1（不进行平均）、2、4、8；多通道时，每个通道接收 N 个数据（平均滤波个数）再切换通道。

### ADC是什么

- `ADC` 的英文全称是`Analog / Digital Converter`，是将模拟信号转换为数字信号的转换器，ADC是单片机读取传感器信号的常见方式。
- 日常生活中的信号，例如光照强度，声波，电池电压 这些都是模拟值。 想对模拟信号(电压，光照强度，声波)进行测量，用数字信号进行表达，这个时候需要**ADC** 模拟数字信号转换器。

## ADC通用方法集合

&emsp;&emsp;`ADC`接口定义了完成`ADC`传输的通用方法集合，包括：

- `ADC`初始化
- 设置平均算法采样次数
- 读取`ADC`采样数据

## ADC引脚定义

&emsp;&emsp;有7个引脚有`ADC`功能，分别对应`ADC`的7个通道。（注意，在`Waffle Nano`中 `9`、`10`号引脚作为与板载传感器沟通的主要线路,`3`,`4`号引脚作为`REPL`的主要线路，**这四个引脚已被使用，无法用作别的功能**）

| 引脚   | 功能 |
| ------ | ---- |
| pin 12 | ADC0 |
| pin 4  | ADC1 |
| pin 5  | ADC2 |
| pin 7  | ADC3 |
| pin 9  | ADC4 |
| pin 11 | ADC5 |
| pin 13 | ADC6 |

## machine.ADC API详解

&emsp;&emsp;使用 `from machine import ADC` 导入 `machine` 模块的模数转换类 `ADC`

&emsp;&emsp;再使用 `TAB` 按键来查看 `ADC` 中所包含的内容：

```python
>>>from machine import ADC
>>>ADC.
read            EQU_MODEL_1     EQU_MODEL_2     EQU_MODEL_4
EQU_MODEL_8     EQU_MODEL_BUTT  equ
```

### 宏定义

&emsp;&emsp;下面的宏定义用于配置pin，也就是将对应编号的真实的管脚配置成输入或者输出或者其他模式。

| 宏定义               | 含义                    |
| -------------------- | ----------------------- |
| ADC.EQU_MODEL_1      | 平均算法的采样次数为1次 |
| ADC.EQU_MODEL_2      | 平均算法的采样次数为2次 |
| ADC.EQU_MODEL_4      | 平均算法的采样次数为4次 |
| ADC.EQU_MODEL_8      | 平均算法的采样次数为8次 |
| ADC.EQU_MODEL_BUTT   | 平均算法的采样次数阀值  |

### 类

&emsp;&emsp;class machine.ADC(channel)

可传入参数：

- `channel`：ADC通道

### 构建ADC

&emsp;&emsp;示例：

```python
>>> from machine import ADC,Pin
>>> adc=ADC(Pin(7)) #将Pin 7 设置为ADC
```

### 函数

&emsp;&emsp;在接下来的示例中, 构造`ADC`对象adc来列举其函数。

```python
adc=ADC(Pin(7))
```

#### 设置平均算法采样次数

&emsp;&emsp;`adc.equ(mode)`

&emsp;&emsp;函数说明：设置平均算法采样次数，返回True代表设置成功。

&emsp;&emsp;mode ：

- ADC.EQU_MODEL_1 — 采样1次
- ADC.EQU_MODEL_2 — 采样2次
- ADC.EQU_MODEL_4 — 采样4次
- ADC.EQU_MODEL_8 — 采样8次
- ADC.EQU_MODEL_BUTT — 采样阀值

&emsp;&emsp;示例：

```python
>>> from machine import ADC,Pin
>>> adc=ADC(Pin(7)) #将Pin 7 设置为ADC
>>> adc.equ(ADC.EQU_MODEL_8) #设置ADC平均算法的采样次数为8次
True
```

#### ADC测量电压范围

&emsp;&emsp;默认电压为`3.3V`，引脚不可能超过管脚电压**3.3v**。

#### ADC采样

&emsp;&emsp;`adc.read()`

&emsp;&emsp;函数说明：进行`ADC`采样，返回采样数据

&emsp;&emsp;示例：

```python
>>>from machine import ADC,Pin
>>>adc=ADC(Pin(7)) #将GPIO 7 设置为ADC
>>>adc.read()#采样
996
```

## 示例

&emsp;&emsp;`将waffle nano`的一些`GPIO`引脚设置成`ADC`，看其电压变化如何。

```python
from machine import ADC,Pin
adc=ADC(Pin(7))
adc.equ(ADC.EQU_MODEL_8)
adc.read()
```

- 第一行导入 `machine` 模块的硬件类 `Pin` 和数模转换类 `ADC`
- 第二行把`waffle nano` 7号引脚设置为默认的`GPIO` 功能，并作为参数创建为 `ADC` 对象
- 第三行设置 `ADC` 的平均计算的采样次数为 `8` 次
- 第四行读取采样值