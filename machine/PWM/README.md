# PWM脉宽调制

- [概要](#概要)
    - [什么是PWM脉宽调制技术](#什么是pwm脉宽调制技术)
    - [占空比](#占空比)
    - [频率](#频率)
    - [PWM脉宽调制通用方法集合](#pwm脉宽调制通用方法集合)
- [PWM引脚定义](#pwm引脚定义)
- [machine.PWM API详解](#machinepwm-api详解)
    - [类](#类)
    - [创建PWM对象](#创建pwm对象)
    - [函数](#函数)
      - [初始化](#初始化)
      - [频率](#频率-1)
      - [占空比](#占空比-1)
      - [释放资源](#释放资源)
- [示例](#示例)

## 概要

- 脉宽调制技术的原理与属性（占空比，频率）
- 通过PWM脉宽调节技术控制LED的亮度的演示实例。

### 什么是PWM脉宽调制技术

- PWM的全称为Pulse Width Modulation脉冲宽度调节，是把模拟信号调制成脉波的技术。

### 占空比

&emsp;&emsp;在一个周期内，高电平时间占总体周期的比例，称之为**占空比 （duty）**。

![pwm](Image/pwm.jpg)

&emsp;&emsp;例如PWM的控制周期为100ms，其中20ms为高电平，80ms为低电平，则占空比就是 20/100 = 20%。

&emsp;&emsp;注意有时候占空比有时候在嵌入式并不是百分比，而是参考其**分辨率**。

&emsp;&emsp;分辨率越高，也就意味着你可以调节的亮度的档位也就越高，引脚输出的**平均电压**处于0-3.3v之间 划分成1024份，你可以取其任意一个。

### 频率

&emsp;&emsp;PWM的第二个属性是**频率**， 频率为控制周期T的倒数。在上面这个例子里面，100ms就是控制周期，那频率就是`1s / 0.1s = 10HZ`

&emsp;&emsp;频率的取值范围由硬件决定。

### PWM脉宽调制通用方法集合

&emsp;PWM脉宽调制定义了操作PWM的通用方法集合，包括：

- 获取、释放设备句柄
- 读写数据、获取和设置波特率
- 获取和设置设备属性。

## PWM引脚定义

&emsp;&emsp;共有14个引脚有PWM功能:

&emsp;&emsp;总共6组PWM。（注意，在Waffle Nano中3,4号引脚为python REPL代码交互用引脚，已被占用无法进行二次调用）

| 引脚                 | 功能     |
| -------------------- | -------- |
| pin 7、pin 10         | PWM0 OUT |
| pin 8、pin 9 | PWM1 OUT |
| pin 2、pin 5、pin 11 | PWM2 OUT |
| pin 0、pin 6、pin 12 | PWM3 OUT |
| pin 1        | PWM4 OUT |
| pin 14        | PWM5 OUT |

## machine.PWM API详解

&emsp;&emsp;**PWM可在所有输出引脚上启用。但其存在局限：须全部为同一频率，且仅有8个通道。**

&emsp;&emsp;使用`from machine import PWM`导入 `machine` 模块的脉宽调制类 `PWM`

&emsp;&emsp;再使用 `TAB` 按键来查看 `PWM` 中所包含的内容：

```python
>>> from machine import PWM
>>>PWM.
deinit          duty            freq            init
```

### 类

&emsp;&emsp;class machine.PWM(id, Pin(),freq,duty)

&emsp;&emsp;id ：PWM组号。

&emsp;&emsp;Pin() ：有PWM组号相应功能的引脚。

&emsp;&emsp;freq ：频率，频率范围由硬件决定，最低输出频率为611赫兹，最高数据频率为65535赫兹。

&emsp;&emsp;duty ：占空比，介于0至100的闭开区间。

### 创建PWM对象

&emsp;&emsp;利用类对象创建PWM

&emsp;&emsp;示例：

```python
>>> from machine import Pin,PWM
>>> p = PWM(0, Pin(7),freq=2442,duty=50) #构建PWM 0对象，频率为2442赫兹，占空比为50%
```

### 函数

&emsp;&emsp;在接下来的示例中, 构造`id=0`的`PWM`对象来列举PWM对象的函数。

```python
>>>from machine import PWM
>>>pwm = PWM(0,Pin(7))#构建PWM0
```

#### 初始化

&emsp;&emsp;pwm.init(freq ,duty)

&emsp;&emsp;函数说明：初始化PWM脉宽调制

&emsp;&emsp;参数含义同上文类构造一致

```python
>>> from machine import Pin,PWM
>>> pwm = PWM(0,Pin(7))
>>> pwm.init(freq=2442,duty=50)#初始化PWM频率为2442，占空比为50
```

#### 频率

&emsp;&emsp;pwm.freq()

&emsp;&emsp;函数说明：设置PWM频率

&emsp;&emsp;频率范围由硬件决定

```python
>>> from machine import Pin,PWM
>>> pwm = PWM(0,Pin(7))#创建PWM 0对象
>>> pwm.freq(2442)#设置PWM频率为2442
```

#### 占空比

&emsp;&emsp;pwm.duty()

&emsp;&emsp;函数说明：设置PWM占空比

&emsp;&emsp;占空比为50%

```python
>>> from machine import Pin,PWM
>>> pwm = PWM(0,Pin(7))#创建PWM 0对象
>>> pwm.duty(50)#设置PWM占空比为50%
```

#### 释放资源

&emsp;&emsp;pwm.deinit()

&emsp;&emsp;函数说明：pwm使用完了之后，需要销毁，释放资源，否则引脚将继续保持在PWM模式

```python
>>> from machine import Pin,PWM
>>> pwm = PWM(0)#创建PWM 0对象
>>> pwm.deinit()#释放pwm 0资源
```

## 示例

&emsp;&emsp;将waffle nano的一些GPIO引脚构造成PWM,设置其频率和占空比。

```python
from machine import Pin,PWM
p = PWM(0, Pin(7))
p.freq(2442)
p.freq(3442)
p.duty(50)
p.deinit()
```

&emsp;&emsp;第一行导入`machine`模块的硬件类`Pin`和脉宽调制类`PWM`

&emsp;&emsp;第二行创建`PWM`对象

- 0 — 构造的是`PWM 0`
- Pin(7) — GPIO 7号引脚实现PWM 0功能

&emsp;&emsp;三四行设置PWM 0的频率

&emsp;&emsp;第五行设置PWM 0的占空比为50%

&emsp;&emsp;第六行释放PWM 0资源