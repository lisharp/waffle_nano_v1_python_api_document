# `ustruct` – 打包和解压缩原始数据类型

  - [概要](#概要)
  - [`ustruct` API详解](#ustruct-api详解)
    - [函数](#函数)

## 概要

&emsp;&emsp;在网络通信当中，大多传递的数据是以二进制流存在的。当传递字符串时，不必担心太多的问题，而当传递诸如int、char之类的基本数据的时候，就需要有一种机制将某些特定的结构体类型打包成二进制流的字符串然后再网络传输，而接收端也应该可以通过某种机制进行解包还原出原始的结构体数据。该模块就提供了这样的机制，该模块的主要作用就是对按照指定格式将Python数据转换为字符串,该字符串为字节流,如网络传输时,不能传输int,此时先将int转化为字节流,然后再发送或者按照指定格式将字节流转换为Python指定的数据类型。

&emsp;&emsp;支持的大小/字节顺序前缀: `@`, `<`, `>`, `!`.

&emsp;&emsp;支持的格式编码: `b`, `B`, `h`, `H`, `i`, `I`, `l`, `L`, `q`, `Q`, `s`, `P`, `f`, `d` 后两个取决于浮点支持）。

## `ustruct` API详解

&emsp;&emsp;使用`import ustruct`导入`ustruct`模块

&emsp;&emsp;再使用`TAB` 按键来查看`ustruct`中所包含的内容：

```python
>>> import ustruct
>>> ustruct.
__name__        calcsize        pack            pack_into
unpack          unpack_from
```

### 函数

- `ustruct.calcsize`(*fmt*)

  函数说明：返回需存入给定 *fmt* 的字节数量。

  示例：

  ```python
  >>> import ustruct
  >>> ustruct.calcsize('b')#返回给的b格式字节数量
  1
  >>> ustruct.calcsize('d')#返回给的d格式字节数量
  8
  ```

- `ustruct.pack`(*fmt*, *v1*, *v2*, *...*)

  函数说明：根据格式字符串fmt，打包 *v1, v2, …* 值。返回值为一个解码该值的字节对象。

  示例：

  ```python
  >>> import ustruct
  >>> ustruct.pack('b',1)#根据格式字符b,打包整数1，返回解码1的字节对象
  b'\x01
  ```

- `ustruct.pack_into`(*fmt*, *buffer*, *offset*, *v1*, *v2*, *...*)

  函数说明：根据格式字符串fmt，将 *v1, v2, …* 值打包进从 *offset* 开始的缓冲区。从缓冲区的末端计数， *offset* 可能为负值。

  示例：

  ```python
  >>> import ustruct
  >>> buf=bytearray(8)#缓冲区
  >>> ustruct.pack_into('d',buf,0,1)#根据格式字符d,打包整数1进从0开始的缓冲区buf
  >>> buf
  bytearray(b'\x00\x00\x00\x00\x00\x00\xf0?')
  ```

- `ustruct.unpack`(*fmt*, *data*)

  函数说明：根据格式字符串 *fmt* 对数据进行解压。返回值为一个解压值元组。

  示例：

  ```python
  >>> import ustruct
  >>> ustruct.pack('d',1)#根据格式字符d,打包1，返回解码1的字节对象
  b'\x00\x00\x00\x00\x00\x00\xf0?'
  >>> ustruct.unpack('d',b'\x00\x00\x00\x00\x00\x00\xf0?')#根据格式字符d对数据b'\x00\x00\x00\x00\x00\x00\xf0?'解压
  (1.0,)
  ```

- `ustruct.unpack_from`(*fmt*, *data*, *offset=0*)

  函数说明：根据格式字符串 *fmt* 解压从 *offset* 开始的数据。从缓冲区的末端计数， *offset* 可能为负值。返回值是一个解压值元组。

    示例：

  ```python
  >>> import ustruct
  >>> buf=bytearray(8)#缓冲区
  >>> ustruct.pack_into('d',buf,0,1)#根据格式字符d,打包整数1进从0开始的缓冲区buf
  >>> buf
  bytearray(b'\x00\x00\x00\x00\x00\x00\xf0?')#格式字符串 d 解压从偏移量为0 开始的数据b'\x00\x00\x00\x00\x00\x00\xf0?'
  >>> ustruct.unpack_from('d',b'\x00\x00\x00\x00\x00\x00\xf0?')
  (1.0,)
  ```
