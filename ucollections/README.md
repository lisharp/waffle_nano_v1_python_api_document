# `ucollections` – 容器

  - [概要](#概要)
  - [`ucollections` API 详解](#ucollections-api-详解)
    - [函数](#函数)
  - [类](#类)

## 概要

&emsp;&emsp;该模块实现相应CPython模块的子集，如下所示。更多信息.

&emsp;&emsp;该模块实现高级集合和容器类型来保存/累积各种对象。

## `ucollections` API 详解

&emsp;&emsp;使用`import ucollections`导入`ucollections`模块

&emsp;&emsp;再使用`TAB` 按键来查看`ucollections`中所包含的内容：

```python
>>> import ucollections
>>> ucollections.
__name__        namedtuple
```

### 函数

### 类

- `ucollections.namedtuple`(*name*, *fields*)

&emsp;&emsp;函数说明:用以创建具有特定名称和字段集的新命名的元组类型的函数。

&emsp;&emsp;`name`：元组名称

&emsp;&emsp;`fields`：元组元素的名称

&emsp;&emsp;命名元组为元组的子类，可以通过数值索引或具有符号字段名的属性访问语法来访问其字段。字段是指定字段名的字符串序列。 

&emsp;&emsp;为实现与CPython的兼容，也可为一个带有空格分隔字段的字符串（但是这样效率较低）。 

&emsp;&emsp;示例:

```python
>>> from ucollections import namedtuple 
>>> MyTuple = namedtuple("MyTuple", ("id", "name")) #创建带字段名的元组
>>> t1 = MyTuple(1, "foo") #为元组中元素赋值
>>> t2 = MyTuple(2, "bar") 
>>> print(t1.name)#通过符号名属性访问元组中元素
foo
>>> print(t2.id)
2
```

